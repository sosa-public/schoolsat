# LoRa Receiver Dongle
This folder includes all design files for a USB LoRa Receiver Dongle for DemoSat reception on only one computer directly.

## Folder structure
**CAD** - holds design files and 3D printing files for dongle case

**HW** - PCB design files

**FW** - firmware for the receiver dongle