#include <SPI.h>
#include <LoRa.h>
#include <EEPROM.h>
#include "base64.hpp"
#include <MemoryFree.h>
#include <SimpleCLI.h>

#define LIMIT
#define SERIAL_BAUD 57600

#define LORA_NSS 10
#define LORA_RST 9
#define LORA_DIO0 3

SimpleCLI cli;

enum Modes
{
  m_csv,
  m_b64,
  m_raw
};

long l_freq = 867000000L; //freq in MHz
byte i_pOut = 14;
char *cPacket;
int i_dataLength = 0;
String sStructure = "i16,i16,i16,i16,i16,i16,i16,i16,i16,u32,i16,f,f,u16,i8,i8,u16,";
byte i_byteLen = 38;
String sCommand;
unsigned long ul_pingMillis = 0;

bool b_rssi = false;
bool b_snr = false;
bool b_crc = true;

bool b_pingCount = false;
Modes mode = m_b64;

struct EEPROMobj
{
  long e_freq;
  Modes e_mode;
  bool e_rssi;
  bool e_snr;
  bool e_crc;
  byte e_pOut;
};

void (*resetFunc)(void) = 0; //reset function

unsigned short crc16(char *data_p, unsigned short length)
{
  unsigned char i;
  unsigned int data;
  unsigned int crc = 0xffff;

  if (length == 0)
    return (~crc);

  do
  {
    for (i = 0, data = (unsigned int)0xff & *data_p++;
         i < 8;
         i++, data >>= 1)
    {
      if ((crc & 0x0001) ^ (data & 0x0001))
        crc = (crc >> 1) ^ 0x1021;
      else
        crc >>= 1;
    }
  } while (--length);

  crc = ~crc;
  data = crc;
  crc = (crc << 8) | (data >> 8 & 0xff);

  return (crc);
}

void saveEEPROM()
{
  EEPROMobj inEEPROM;

  EEPROM.get(0, inEEPROM);

  Serial.println(F("Saving params!!!"));

  if (inEEPROM.e_freq != l_freq)
    EEPROM.put(0, l_freq);
  else
    Serial.println(F("\tFreq same as stored in EEPROM"));

  if (inEEPROM.e_mode != mode)
    EEPROM.put(sizeof(l_freq), mode);
  else
    Serial.println(F("\tMode same as stored in EEPROM"));

  if (inEEPROM.e_rssi != b_rssi)
    EEPROM.put(sizeof(l_freq) + sizeof(mode), b_rssi);
  else
    Serial.println(F("\tRSSI settings same as stored in EEPROM"));

  if (inEEPROM.e_snr != b_snr)
    EEPROM.put(sizeof(l_freq) + sizeof(mode) + sizeof(b_rssi), b_snr);
  else
    Serial.println(F("\tSNR settings same as stored in EEPROM"));

  if (inEEPROM.e_crc != b_crc)
    EEPROM.put(sizeof(l_freq) + sizeof(mode) + sizeof(b_rssi) + sizeof(b_snr), b_crc);
  else
    Serial.println(F("\tCRC settings same as stored in EEPROM"));

  if (inEEPROM.e_pOut != i_pOut)
    EEPROM.put(sizeof(l_freq) + sizeof(mode) + sizeof(b_rssi) + sizeof(b_snr) + sizeof(b_crc), i_pOut);
  else
    Serial.println(F("\tTX power same as stored in EEPROM"));

  Serial.println(F("Saving to EEPROM completed"));
}

void readEEPROM(bool useValues)
{
  Serial.println(F("Reading settings from EEPROM"));

  EEPROMobj inEEPROM;

  EEPROM.get(0, inEEPROM);
  Serial.print(F("\tFrequency: "));
  Serial.println(inEEPROM.e_freq);
  Serial.print(F("\tMode: "));
  switch (mode)
  {
    case m_csv:
      Serial.println(F("CSV"));
      break;
    case m_b64:
      Serial.println(F("Base64"));
      break;
    case m_raw:
      Serial.println(F("RAW"));
      break;
  }
  Serial.print(F("\tRSSI output: "));
  inEEPROM.e_rssi ? Serial.println("ON") : Serial.println("OFF");
  Serial.print(F("\tSNR output: "));
  inEEPROM.e_snr ? Serial.println("ON") : Serial.println("OFF");
  Serial.print(F("\tCRC check: "));
  inEEPROM.e_crc ? Serial.println("ON") : Serial.println("OFF");
  Serial.print(F("\tTX power: "));
  Serial.println(inEEPROM.e_pOut);

  Serial.println(F("Settings sucessfully read"));
  if (useValues)
  {
    l_freq = inEEPROM.e_freq;
    mode = inEEPROM.e_mode;
    b_rssi = inEEPROM.e_rssi;
    b_snr = inEEPROM.e_snr;
    b_crc = inEEPROM.e_crc;
    i_pOut = inEEPROM.e_pOut;

    LoRa.setFrequency(l_freq);
    LoRa.setTxPower(i_pOut);

    Serial.println(F("EEPROM values sucessfully used"));
  }
}

void serialEvent()
{
  while (Serial.available())
  {
    char inChar = (char)Serial.read();
    if (inChar != '\r')
      sCommand += inChar;
    if (inChar == '\n')
    {
      Serial.print("# ");
      Serial.println(sCommand);
      cli.parse(sCommand);
      sCommand = "";
    }
  }
}

void setup()
{
  readEEPROM(1);
  initCmds();
  Serial.begin(SERIAL_BAUD);

  sStructure.reserve(100);
  sCommand.reserve(100);

  Serial.println(F("LoRa Receiver"));

  LoRa.setPins(LORA_NSS, LORA_RST, LORA_DIO0);
  if (!LoRa.begin(l_freq))
    Serial.println(F("Starting LoRa failed!"));
  else
  {
    LoRa.setTxPower(i_pOut);
    Serial.print(F("Frequency "));
    Serial.println(l_freq);
    Serial.print(F("RSSI output "));
    Serial.println(b_rssi ? "ON" : "OFF");
    Serial.print(F("SNR output "));
    Serial.println(b_snr ? "ON" : "OFF");
    Serial.print(F("CRC check "));
    Serial.println(b_crc ? "ON" : "OFF");
    Serial.print(F("Power output set to "));
    Serial.print(i_pOut);
    Serial.println(F("dBm"));


    LoRa.onReceive(onReceive);

    // put the radio into receive mode
    LoRa.receive();
  }
}

void onReceive(int packetSize)
{
  recievePacket(packetSize);
}

void loop()
{
  //ping timeout
  if (b_pingCount && millis() - ul_pingMillis >= 3000)
  {
    Serial.println("Timeout");
    b_pingCount = false;
  }
}
