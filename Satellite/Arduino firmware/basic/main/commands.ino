#include<SimpleCLI.h>

void pingRadioCommand(cmd *c);
void trxDelayCommand(cmd *c);
void epsCommand(cmd *c);
void obcCommand(cmd *c);
void gpsCommand(cmd *c);

static void initCmds()
{
  Command pingRadio;
  Command trxCmd;
  Command EpsCmd;
  Command ObcCmd;
  Command GpsCmd;

  //radio ping
  pingRadio = radioCli.addCmd("p/ing, test", pingRadioCommand);

  //set trx delay
  trxCmd = radioCli.addBoundlessCmd("trx", trxCommand);

  //eps commands
  EpsCmd = radioCli.addBoundlessCmd("eps", epsCommand);

  //obc commands
  ObcCmd = radioCli.addBoundlessCmd("obc", obcCommand);

  //gps commands
  GpsCmd = radioCli.addBoundlessCmd("gps", gpsCommand);
}

void pingRadioCommand(cmd *c)
{
  send_LoRa("pong", 4);
}

void trxCommand(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();

  if (argVal.equals(F("hk")) || argVal.equals(""))
  {

  }
  else if (argVal.equals("lor") || argVal.equals("lora"))
  {
    Argument arg2 = cmd.getArgument(1);
    String arg2s = arg2.getValue();
    if (arg2s.equals("on") || arg2s.equals("1"))
    {
      LoRa_beacon_on = true;
      }
    else if (arg2s.equals("off") || arg2s.equals("0"))
    {
      LoRa_beacon_on = false;
    }
  }

  else if (argVal.equals(F("dly")))
  {
    int argc = cmd.countArgs();
    if (argc == 2)
    {
      unsigned int newInterval = cmd.getArg(1).getValue().toInt();
      if (newInterval >= 5)
        LoRa_interval = newInterval * 1000;
    }
  }
}

void epsCommand(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();

  if (argVal.equals(F("hk")) || argVal.equals(""))
  {
    batt_adc = analogRead(A0);
    float voltage = batt_adc * (5.0 / 1023.0);
    Serial.print("Batt volatge is: ");
    Serial.println(voltage);

  }
  else if (argVal.equals("set"))
  {
    int argc = cmd.countArgs();
    if (argc == 2)
    {
      String arg_on = cmd.getArg(1).getValue();

      if (arg_on.equals("on") || arg_on.equals("1"))
      {
        digitalWrite(GREEN_LED, HIGH);
      }
      else if (arg_on.equals("off") || arg_on.equals("0"))
      {
        digitalWrite(GREEN_LED, LOW);
      }
    }
  }
}

void obcCommand(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();

  if (argVal.equals(F("hk")) || argVal.equals(""))
  {
    Serial.print((long)(millis() / 1000));
    Serial.print(",");
    Serial.print(gx);
    Serial.print(",");
    Serial.print(gy);
    Serial.print(",");
    Serial.print(gz);
    Serial.print(",");
    Serial.print(ax);
    Serial.print(",");
    Serial.print(ay);
    Serial.print(",");
    Serial.print(az);
    Serial.print(",");
    Serial.print(mx);
    Serial.print(",");
    Serial.print(my);
    Serial.print(",");
    Serial.print(mz);
    Serial.print(",");
    Serial.print(pressure);
    Serial.print(",");
    Serial.print(temp);
    Serial.print(",");
    Serial.println(ntc_temp);
  }
}

void gpsCommand(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();

  if (argVal.equals(F("hk")) || argVal.equals(""))
  {
    Serial.print(lat, 6);
    Serial.print(",");
    Serial.print(lon, 6);
    Serial.print(",");
    Serial.print(alt);
    Serial.print(",");
    Serial.print(sats);
    Serial.print(",");
    Serial.println(fix);
  }
}
