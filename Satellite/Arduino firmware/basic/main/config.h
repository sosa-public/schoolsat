//LED pin definition
#define RED_LED A12 //GPS fix indicator
#define ORANGE_LED A14 //LoRa transmitt indicator
#define GREEN_LED 32 //power chanel emulator

//LoRa setup
#define LORA_NSS 34
#define LORA_RST 36
#define LORA_DIO0 2
#define LORA_FREQUENCY 867.0 //in MHz

//SD card
#define SD_SS 46

//NTC thermistor
#define C1 0.001125308852122
#define C2 0.000234711863267
#define C3 0.000000085663516
#define NTC_R 10E3 // 10k ohm thermistor
#define NTC A1 //ntc readings at A1 analog pin


/**********************************************
  Advanced sensor setup
 **********************************************/

//BME280 serial debug
//#define BME_debug

//BMP180 settings
// Ultra Low Power       OSS = 0, OSD =  5ms
// Standard              OSS = 1, OSD =  8ms
// High                  OSS = 2, OSD = 14ms
// Ultra High Resolution OSS = 3, OSD = 26ms
#define OSS 3                      // Set oversampling setting
#define OSD 26                     // with corresponding oversampling delay

//BMP180 serial debug
//#define BMP_debug

//MPU6050 settings
#define MPU_ACCEL_RESOLUTION 0x18 //0x00 - 2g; 0x08 - 4g; 0x10 - 8g; 0x18 - 16g 
#define MPU_GYRO_RESOLUTION 0 //0x00 - 250°/s; 0x08 - 500°/s; 0x10 - 1000°/s; 0x18 - 2000°/s

//QMC5883L settings
#define QMC_Mode_Standby    0b00000000
#define QMC_Mode_Continuous 0b00000001

#define QMC_ODR_10Hz        0b00000000
#define QMC_ODR_50Hz        0b00000100
#define QMC_ODR_100Hz       0b00001000
#define QMC_ODR_200Hz       0b00001100

#define QMC_RNG_2G          0b00000000
#define QMC_RNG_8G          0b00010000

#define QMC_OSR_512         0b00000000
#define QMC_OSR_256         0b01000000
#define QMC_OSR_128         0b10000000
#define QMC_OSR_64          0b11000000

//HMC5883 settings
#define HMC_Mode_Standby    0b00000011
#define HMC_Mode_Continuous 0b00000000
#define HMC_Mode_Singe      0b00000001

#define HMC_AVG_1           0b00000000
#define HMC_AVG_2           0b00100000
#define HMC_AVG_4           0b01000000
#define HMC_AVG_8           0b01100000

#define HMC_ODR_0_75        0b00000000
#define HMC_ODR_1_5         0b00000100
#define HMC_ODR_3           0b00001000
#define HMC_ODR_7_5         0b00001100
#define HMC_ODR_15          0b00010000
#define HMC_ODR_30          0b00010100
#define HMC_ODR_75          0b00011000

#define HMC_RNG_0_88G       0b00000000
#define HMC_RNG_1_30G       0b00100000
#define HMC_RNG_1_90G       0b01000000
#define HMC_RNG_2_50G       0b01100000
#define HMC_RNG_4_00G       0b10000000
#define HMC_RNG_4_70G       0b10100000
#define HMC_RNG_5_60G       0b11000000
#define HMC_RNG_8_10G       0b11100000

#define HMC_LSB_0_88G       1370
#define HMC_LSB_1_30G       1090
#define HMC_LSB_1_90G       820
#define HMC_LSB_2_50G       660
#define HMC_LSB_4_00G       440
#define HMC_LSB_4_70G       390
#define HMC_LSB_5_60G       330
#define HMC_LSB_8_10G       230
