#include <Wire.h>
#include <Adafruit_INA219.h>

Adafruit_INA219 ina219_A(0x40);
Adafruit_INA219 ina219_B(0x41);
Adafruit_INA219 ina219_C(0x44);
Adafruit_INA219 ina219_D(0x45);

bool solar_init() {
  // Initialize the INA219.
  // By default the initialization will use the largest range (32V, 2A).  However
  // you can call a setCalibration function to change this range (see comments).
  if (!ina219_C.begin()) {
    Serial.println("Failed to find INA219 chip A");
  }
  if (!ina219_D.begin()) {
    Serial.println("Failed to find INA219 chip B");
  }
  if (!ina219_B.begin()) {
    Serial.println("Failed to find INA219 chip C");
  }
  if (!ina219_A.begin()) {
    Serial.println("Failed to find INA219 chip D");
  }

  // To use a slightly lower 32V, 1A range (higher precision on amps):
  // ina219.setCalibration_32V_1A();
  // Or to use a lower 16V, 400mA range (higher precision on volts and amps):
  // ina219.setCalibration_16V_400mA();

  Serial.println("Measuring voltage and current with INA219 ...");

  return true;
}

/*
  ch is from 1 to 4
*/
float solar_get_power_mW(int ch) {
  switch (ch) {
    case 1:
      return ina219_A.getPower_mW();
      break;
    case 2:    
      return ina219_B.getPower_mW();
      break;
    case 3:
      return ina219_C.getPower_mW();
      break;
    case 4:
      return ina219_D.getPower_mW();
      break;
    default:
      return -9999.9; // non-existing channel
  }
}
