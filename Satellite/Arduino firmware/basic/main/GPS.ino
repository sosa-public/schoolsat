#include "config.h"

TinyGPSCustom fixQuality(gps, "GPGGA", 6);
TinyGPSCustom satsCount(gps, "GPGGA", 7);

void getGPS(float *lat_in, float *lon_in, uint16_t *alt_in, uint8_t *sats_in, int8_t *fix_in)
{
  *fix_in = getGpsQuality(fixQuality.value());
  if (*fix_in > 0 && gps.time.age() < 1000)
  {
    digitalWrite(RED_LED, LOW);
    *lat_in = gps.location.lat();
    *lon_in = gps.location.lng();
    *alt_in = gps.altitude.meters();
    *fix_in = getGpsQuality(fixQuality.value());
    *sats_in = getGpsSats(satsCount.value());

  }
  else
  {
    digitalWrite(RED_LED, HIGH);
    *lat_in = 0;
    *lon_in = 0;
    *alt_in = 0;
    *fix_in = 0;
    *sats_in = 0;
  }
}

int getGpsQuality(const char* value)
{
  String quality(value);
  if (quality.equals(NULL))
  {
    return 0;
  }
  else
  {
    return quality.toInt();
  }
}

int getGpsSats(const char* value)
{
  String sats_s(value);
  if (sats_s.toInt() < 1 || sats_s.toInt() > 15)
  {
    return 0;
  }
  else
  {
    return sats_s.toInt();
  }
}
