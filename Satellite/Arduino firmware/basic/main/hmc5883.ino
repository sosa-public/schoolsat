#include "config.h"

byte _hmc_address;

void setConfig_hmc(uint16_t mode, uint16_t odr, uint16_t rng, uint16_t avg) {
  // Configuration register A
  write8(_hmc_address, 0x00, odr | avg);
  // Configuration register B
  write8(_hmc_address, 0x01, rng);
  // Mode register
  write8(_hmc_address, 0x02, mode);
}

void init_hmc(byte address) {
  _hmc_address = address;
  Serial.print("hmc5883L init started...");

  //Define Set/Reset period
  write8(_hmc_address, 0x0B, 0x01);
  // Continuous measurement, 30 Hz data rate, 2.5 Gauss range, 1 sample average
  setConfig_hmc(HMC_Mode_Continuous, HMC_ODR_30, HMC_RNG_2_50G, HMC_AVG_1);

  Serial.println("DONE");
}

void get_hmc(uint16_t* x, uint16_t* y, uint16_t* z) {
  *x = readS16(_hmc_address, 0x03);
  *y = readS16(_hmc_address, 0x07);
  *z = readS16(_hmc_address, 0x05);
}
