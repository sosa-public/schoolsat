#include "config.h"

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// flag to indicate that a packet was sent or received
volatile bool receivedFlag = false;

// this function is called when a complete packet
// is transmitted or received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if (!enableInterrupt) {
    return;
  }
  // we sent aor received  packet, set the flag
  receivedFlag = true;
}
void init_LoRa() {
  int state;
  Serial.println("Starting LoRa...");
  if (LORA_FREQUENCY < 863 || LORA_FREQUENCY > 868) {
    Serial.println("Please set frequency within 863-868MHz");
    lora = false;
  } else {
    Serial.print("Configurin frequency ");
    Serial.println(LORA_FREQUENCY);
    state = radio.begin(LORA_FREQUENCY, 125, 7, 5, 0x12);
    if (state != RADIOLIB_ERR_NONE) {
        Serial.print("Starting LoRa failed! Radio state: ");
        Serial.println(state);
      lora = false;
    } else {
      radio.setDirectAction(setFlag);
      // radio.setDio0Action(setFlag);
      state = radio.startReceive();
      if (state == RADIOLIB_ERR_NONE) {
        Serial.println("Successful LoRa start");
        lora = true;
      } else {
        Serial.print("Starting LoRa failed! Radio state: ");
        Serial.println(state);
        radio.startReceive();
      }
    }
  }
}

void send_LoRa(uint8_t trxInput[], int trxInputSize) {
  enableInterrupt = false;
  digitalWrite(ORANGE_LED, HIGH);

  int transmissionState = radio.startTransmit(trxInput, trxInputSize);
  delay(100);

  digitalWrite(ORANGE_LED, LOW);
  radio.startReceive();
  enableInterrupt = true;
}

void receive_LoRa() {
  if (receivedFlag) {
    enableInterrupt = false;  //turn off interrupts
    receivedFlag = false;     //reset
    String sReceived;
    int state = radio.readData(sReceived);

    if (state == RADIOLIB_ERR_NONE) {
      Serial.print("<< ");
      Serial.println(sReceived);
      radioCli.parse(sReceived);
    }
    radio.startReceive();  // back to receive mode
    enableInterrupt = true;
  }
}
