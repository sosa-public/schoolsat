#include "config.h"

byte _bme_address;

struct BME280_Calibration_Data {
  uint16_t dig_T1 = 0;
  int16_t dig_T2 = 0;
  int16_t dig_T3 = 0;

  uint16_t dig_P1 = 0;
  int16_t dig_P2 = 0;
  int16_t dig_P3 = 0;
  int16_t dig_P4 = 0;
  int16_t dig_P5 = 0;
  int16_t dig_P6 = 0;
  int16_t dig_P7 = 0;
  int16_t dig_P8 = 0;
  int16_t dig_P9 = 0;

  uint8_t dig_H1 = 0;
  int16_t dig_H2 = 0;
  uint8_t dig_H3 = 0;
  int16_t dig_H4 = 0;
  int16_t dig_H5 = 0;
  int8_t dig_H6 = 0;
};

int32_t t_fine;

BME280_Calibration_Data bme_cal_data;

void readBMESensorCoefficients(void) {
  bme_cal_data.dig_T1 = read16_LE(_bme_address, 0x88);
  bme_cal_data.dig_T2 = readS16_LE(_bme_address, 0x8A);
  bme_cal_data.dig_T3 = readS16_LE(_bme_address, 0x8C);
  bme_cal_data.dig_P1 = read16_LE(_bme_address, 0x8E);
  bme_cal_data.dig_P2 = readS16_LE(_bme_address, 0x90);
  bme_cal_data.dig_P3 = readS16_LE(_bme_address, 0x92);
  bme_cal_data.dig_P4 = readS16_LE(_bme_address, 0x94);
  bme_cal_data.dig_P5 = readS16_LE(_bme_address, 0x96);
  bme_cal_data.dig_P6 = readS16_LE(_bme_address, 0x98);
  bme_cal_data.dig_P7 = readS16_LE(_bme_address, 0x9A);
  bme_cal_data.dig_P8 = readS16_LE(_bme_address, 0x9C);
  bme_cal_data.dig_P9 = readS16_LE(_bme_address, 0x9E);
  bme_cal_data.dig_H1 = read8(_bme_address, 0xA1);
  bme_cal_data.dig_H2 = readS16_LE(_bme_address, 0xE1);
  bme_cal_data.dig_H3 = read8(_bme_address, 0xE3);
  bme_cal_data.dig_H4 = (read8(_bme_address, 0xE4) << 4) | (read8(_bme_address, 0xE4 + 1) & 0xF);
  bme_cal_data.dig_H5 = (read8(_bme_address, 0xE5 + 1) << 4) | (read8(_bme_address, 0xE5) >> 4);
  bme_cal_data.dig_H6 = (int8_t)read8(_bme_address, 0xE6);
}

void init_BME(uint8_t bme_address) {
  _bme_address = bme_address;

  Serial.print("BME280 init started...");
  readBMESensorCoefficients();

#if defined(BME_debug)
  Serial.print("T1 = ");
  Serial.println(bme_cal_data.dig_T1);
  Serial.print("T2 = ");
  Serial.println(bme_cal_data.dig_T2);
  Serial.print("T3 = ");
  Serial.println(bme_cal_data.dig_T3);

  Serial.print("P1 = ");
  Serial.println(bme_cal_data.dig_P1);
  Serial.print("P2 = ");
  Serial.println(bme_cal_data.dig_P2);
  Serial.print("P3 = ");
  Serial.println(bme_cal_data.dig_P3);
  Serial.print("P4 = ");
  Serial.println(bme_cal_data.dig_P4);
  Serial.print("P5 = ");
  Serial.println(bme_cal_data.dig_P5);
  Serial.print("P6 = ");
  Serial.println(bme_cal_data.dig_P6);
  Serial.print("P7 = ");
  Serial.println(bme_cal_data.dig_P7);
  Serial.print("P8 = ");
  Serial.println(bme_cal_data.dig_P8);
  Serial.print("P9 = ");
  Serial.println(bme_cal_data.dig_P9);

  Serial.print("H1 = ");
  Serial.println(bme_cal_data.dig_H1);
  Serial.print("H2 = ");
  Serial.println(bme_cal_data.dig_H2);
  Serial.print("H3 = ");
  Serial.println(bme_cal_data.dig_H3);
  Serial.print("H4 = ");
  Serial.println(bme_cal_data.dig_H4);
  Serial.print("H5 = ");
  Serial.println(bme_cal_data.dig_H5);
  Serial.print("H6 = ");
  Serial.println(bme_cal_data.dig_H6);
#endif

  // Set Humidity oversampling to 1
  write8(_bme_address, 0xF2, 0x01);  // Set before CONTROL (DS 5.4.3)
  write8(_bme_address, 0xF4, 0x3F);

  Serial.println("DONE");
}

int16_t bme_readTemperature() {

  int32_t var1, var2, T;
  int32_t adc_T = read24(_bme_address, 0xFA);

  adc_T >>= 4;

  var1 = ((((adc_T >> 3) - ((int32_t)bme_cal_data.dig_T1 << 1))) * ((int32_t)bme_cal_data.dig_T2)) >> 11;
  var2 = (((((adc_T >> 4) - ((int32_t)bme_cal_data.dig_T1)) * ((adc_T >> 4) - ((int32_t)bme_cal_data.dig_T1))) >> 12) * ((int32_t)bme_cal_data.dig_T3)) >> 14;
  t_fine = var1 + var2;

  T = (t_fine * 5 + 128) >> 8;

  return T;
}

uint32_t bme_readPressure() {

  int64_t var1, var2, P;
  int32_t adc_P = read24(_bme_address, 0xF7);

  adc_P >>= 4;

  var1 = ((int64_t)t_fine) - 128000;
  var2 = var1 * var1 * (int64_t)bme_cal_data.dig_P6;
  var2 = var2 + ((var1 * (int64_t)bme_cal_data.dig_P5) << 17);
  var2 = var2 + (((int64_t)bme_cal_data.dig_P4) << 35);
  var1 = ((var1 * var1 * (int64_t)bme_cal_data.dig_P3) >> 8) + ((var1 * (int64_t)bme_cal_data.dig_P2) << 12);
  var1 = (((((int64_t)1) << 47) + var1)) * ((int64_t)bme_cal_data.dig_P1) >> 33;

  if (var1 == 0) {
    return 0;  // avoid exception caused by division by zero
  }

  P = 1048576 - adc_P;
  P = (((P << 31) - var2) * 3125) / var1;
  var1 = (((int64_t)bme_cal_data.dig_P9) * (P >> 13) * (P >> 13)) >> 25;
  var2 = (((int64_t)bme_cal_data.dig_P8) * P) >> 19;

  P = ((P + var1 + var2) >> 8) + (((int64_t)bme_cal_data.dig_P7) << 4);
  P /= 256;

  return P;
}

uint16_t bme_readHumidity() {

  int32_t adc_H = read16(_bme_address, 0xFD);
  int32_t v_x1_u32r;
  uint32_t H;

  v_x1_u32r = (t_fine - ((int32_t)76800));
  v_x1_u32r = (((((adc_H << 14) - (((int32_t)bme_cal_data.dig_H4) << 20) - (((int32_t)bme_cal_data.dig_H5) * v_x1_u32r)) + ((int32_t)16384)) >> 15) * (((((((v_x1_u32r * ((int32_t)bme_cal_data.dig_H6)) >> 10) * (((v_x1_u32r * ((int32_t)bme_cal_data.dig_H3)) >> 11) + ((int32_t)32768))) >> 10) + ((int32_t)2097152)) * ((int32_t)bme_cal_data.dig_H2) + 8192) >> 14));
  v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) * ((int32_t)(bme_cal_data.dig_H1))) >> 4));
  v_x1_u32r = (v_x1_u32r < 0) ? 0 : v_x1_u32r;
  v_x1_u32r = (v_x1_u32r > 419430400) ? 419430400 : v_x1_u32r;
  H = (v_x1_u32r >> 12);

  return H;
}

void bme_getPTU(uint32_t *p, int16_t *t, uint32_t *u) {
  *p = bme_readPressure();
  *t = bme_readTemperature();
  *u = bme_readHumidity();
}

void bme_getPT(uint32_t *p, int16_t *t) {
  *p = bme_readPressure();
  *t = bme_readTemperature();
}
