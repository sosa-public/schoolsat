#include "config.h"

byte _bmp_address;

struct BMP180_Calibration_Data {
  int16_t ac1 = 0;
  int16_t ac2 = 0;
  int16_t ac3 = 0;
  uint16_t ac4 = 0;
  uint16_t ac5 = 0;
  uint16_t ac6 = 0;

  int16_t b1 = 0;
  int16_t b2 = 0;

  int16_t mb = 0;
  int16_t mc = 0;
  int16_t md = 0;
};

BMP180_Calibration_Data bmp_cal_data;

int32_t x1, x2, x3, b3, b5, b6;
uint32_t b4, b7;

int32_t bmp_get_ut() {
  int32_t UT;

  write8(_bmp_address, 0xF4, 0x2E);
  delay(OSD);
  UT = readS16(_bmp_address, 0xF6);
  return UT;
}

int32_t bmp_get_up() {
  int32_t UP;

  write8(_bmp_address, 0xF4, 0x34 + (OSS << 6));
  delay(OSD);
  UP = read24(_bmp_address, 0xF6);
  UP = UP >> (8 - OSS);
  return UP;
}

int16_t bmp_readTemperature() {
  int16_t T;
  int32_t ut = bmp_get_ut();
  x1 = (ut - bmp_cal_data.ac6) * bmp_cal_data.ac5 >> 15;
  x2 = ((int32_t)bmp_cal_data.mc << 11) / (x1 + bmp_cal_data.md);
  b5 = x1 + x2;
  T = (b5 + 8) >> 4;

#if defined(BMP_debug)
  Serial.print("UT = ");
  Serial.println(ut);
  Serial.print("X1_TEMP = ");
  Serial.println(x1);
  Serial.print("X2_TEMP = ");
  Serial.println(x2);
  Serial.print("B5_TEMP = ");
  Serial.println(b5);
#endif

  return T * 10;  //return in 0.01°C to be the same as result from BMP/BME280
}

int32_t bmp_readPressure() {
  int32_t P;
  int32_t up = bmp_get_up();

  b6 = b5 - 4000;
  x1 = (bmp_cal_data.b2 * (b6 * b6 >> 12)) >> 11;
  x2 = bmp_cal_data.ac2 * b6 >> 11;
  x3 = x1 + x2;
  b3 = (((bmp_cal_data.ac1 * 4 + x3) << OSS) + 2) / 4;

#if defined(BMP_debug)
  Serial.print("UP = ");
  Serial.println(up);
  Serial.print("B6 = ");
  Serial.println(b6);
  Serial.print("X1_PRESS_1 = ");
  Serial.println(x1);
  Serial.print("X2_PRESS_1 = ");
  Serial.println(x2);
  Serial.print("X3_PRESS_1 = ");
  Serial.println(x3);
  Serial.print("B3 = ");
  Serial.println(b3);
#endif
  x1 = bmp_cal_data.ac3 * b6 >> 13;
  x2 = (bmp_cal_data.b1 * (b6 * b6 >> 12)) >> 16;
  x3 = ((x1 + x2) + 2) >> 2;
  b4 = (bmp_cal_data.ac4 * (uint32_t)(x3 + 32768)) >> 15;
  b7 = ((uint32_t)up - b3) * (50000 >> OSS);
  if (b7 < 0x80000000)
    P = (b7 * 2) / b4;
  else
    P = (b7 / b4) * 2;

#if defined(BMP_debug)
  Serial.print("X1_PRESS_2 = ");
  Serial.println(x1);
  Serial.print("X2_PRESS_2 = ");
  Serial.println(x2);
  Serial.print("X3 = ");
  Serial.println(x3);
  Serial.print("B4 = ");
  Serial.println(b4);
  Serial.print("B7 = ");
  Serial.println(b7);
  Serial.print("P = ");
  Serial.println(P);
#endif

  x1 = (P >> 8);
  x1 *= (P >> 8);
  x1 = (x1 * 3038) >> 16;
  x2 = (-7357 * P) >> 16;
  P += (x1 + x2 + 3791) >> 4;

#if defined(BMP_debug)
  Serial.print("X1_PRESS_3 = ");
  Serial.println(x1);
  Serial.print("X2_PRESS_3 = ");
  Serial.println(x2);
#endif

  return P;
}


void init_BMP(uint8_t bmp_address) {
  _bmp_address = bmp_address;

  Serial.print("BMP180 init started...");
  readBMPSensorCoefficients();

#if defined(BMP_debug)
  Serial.print("AC1 = ");
  Serial.println(bmp_cal_data.ac1);
  Serial.print("AC2 = ");
  Serial.println(bmp_cal_data.ac2);
  Serial.print("AC3 = ");
  Serial.println(bmp_cal_data.ac3);
  Serial.print("AC4 = ");
  Serial.println(bmp_cal_data.ac4);
  Serial.print("AC5 = ");
  Serial.println(bmp_cal_data.ac5);
  Serial.print("AC6 = ");
  Serial.println(bmp_cal_data.ac6);

  Serial.print("B1 = ");
  Serial.println(bmp_cal_data.b1);
  Serial.print("B2 = ");
  Serial.println(bmp_cal_data.b2);

  Serial.print("MB = ");
  Serial.println(bmp_cal_data.mb);
  Serial.print("MC = ");
  Serial.println(bmp_cal_data.mc);
  Serial.print("MD = ");
  Serial.println(bmp_cal_data.md);
#endif

  Serial.println("DONE");
  return true;
}
void readBMPSensorCoefficients(void) {
  bmp_cal_data.ac1 = readS16(_bmp_address, 0xAA);
  bmp_cal_data.ac2 = readS16(_bmp_address, 0xAC);
  bmp_cal_data.ac3 = readS16(_bmp_address, 0xAE);
  bmp_cal_data.ac4 = read16(_bmp_address, 0xB0);
  bmp_cal_data.ac5 = read16(_bmp_address, 0xB2);
  bmp_cal_data.ac6 = read16(_bmp_address, 0xB4);
  bmp_cal_data.b1 = readS16(_bmp_address, 0xB6);
  bmp_cal_data.b2 = readS16(_bmp_address, 0xB8);
  bmp_cal_data.mb = readS16(_bmp_address, 0xBA);
  bmp_cal_data.mc = readS16(_bmp_address, 0xBC);
  bmp_cal_data.md = readS16(_bmp_address, 0xBE);
}

void bmp_getPT(uint32_t *p, int16_t *t) {
  *t = bmp_readTemperature();
  *p = bmp_readPressure();
}
