#include "config.h"

byte _qmc_address;

void setMode_qmc(uint16_t mode, uint16_t odr, uint16_t rng, uint16_t osr) {
  write8(_qmc_address, 0x09, mode | odr | rng | osr);
}


void softReset_qmc() {
  write8(_qmc_address, 0x0A, 0x80);
}


void init_qmc(byte address) {
  _qmc_address = address;
  Serial.print("QMC5883L init started...");
  //Define Set/Reset period
  write8(_qmc_address, 0x0B, 0x01);

  setMode_qmc(QMC_Mode_Continuous, QMC_ODR_50Hz, QMC_RNG_2G, QMC_OSR_512);
  
  Serial.println("DONE");
}

void get_qmc(uint16_t* x, uint16_t* y, uint16_t* z) {
  *x = readS16_LE(_qmc_address, 0x00);
  *y = readS16_LE(_qmc_address, 0x02);
  *z = readS16_LE(_qmc_address, 0x04);
}
