
//Read 1 byte from I2C device
uint8_t read8(uint8_t address, uint8_t reg)
{
  uint8_t value;
  Wire.beginTransmission(address);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom((uint8_t)address, (uint8_t)1);

  if (Wire.available() <= 1)
    value = Wire.read();

  return value;
}
//Write 1 byte to I2C device
uint8_t write8(uint8_t address, uint8_t reg, uint8_t value)
{
  Wire.beginTransmission(address);
  Wire.write(reg);
  Wire.write(value);
  Wire.endTransmission();
}

//Read 2 bytes unsigned from I2C
uint16_t read16(uint8_t address, uint8_t reg)
{
  uint16_t value;

  Wire.beginTransmission(address);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom((uint8_t)address, (uint8_t)2);

  if (Wire.available() >= 2)
    value = (Wire.read() << 8) | Wire.read();
  return value;
}

//Read 2 bytes signed from I2C
int16_t readS16(byte address, uint8_t reg)
{
  return (int16_t)read16(address, reg);
}

//Read 2 bytes LE unsigned from I2C
uint16_t read16_LE(byte address, uint8_t code)
{

  uint16_t temp = read16(address, code);
  return (temp >> 8) | (temp << 8);
}

//Read 2 bytes LE signed from I2C
int16_t readS16_LE(byte address, uint8_t code)
{
  return (int16_t)read16_LE(address, code);
}

//Read 3 bytes from I2C device
uint32_t read24(uint8_t address, uint8_t reg)
{
  uint32_t value;

  Wire.beginTransmission(address);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom((uint8_t)address, (uint8_t)3);

  if (Wire.available() >= 3)
  {
    value = Wire.read();
    value <<= 8;
    value |= Wire.read();
    value <<= 8;
    value |= Wire.read();
  }
  return value;
}
