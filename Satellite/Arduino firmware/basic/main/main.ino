#include "config.h"
#include <Wire.h>
#include <RadioLib.h>
#include "base64.hpp"
#include <TinyGPS++.h>
#include <SPI.h>
#include <SD.h>
#include <SimpleCLI.h>

#define CRC_16_POLY 0x1021

unsigned long LoRa_previousMillis;
unsigned long serial_previousMillis;
unsigned int LoRa_interval = 5000;    //radio send interval;
unsigned int serial_interval = 5000;  //serial send interval
boolean LoRa_beacon_on = true;

/**********************************************
  Sensor available, determined at runtime
 **********************************************/
bool bmp = false;  // BMP280 environmental sensor present
bool bme = false;  // BME280 environmental sensor present
bool qmc = false;  // QMC Magnetometer present
bool hmc = false;  // HMC Magnetometer present
bool mpu = false;  // MPU IMU sensor present

/**********************************************
  Activation of function, mark true to activate
 **********************************************/
bool lora = true;     // Radio module present
bool sd_card = true;  // SD Card present
bool solar = true;    // Solar system present

/**********************************************
  Sensor I2C bus addresses
 **********************************************/
byte bmp_address = 0x00;
byte bme280_address = 0x00;
byte qmc_address = 0x00;
byte hmc_address = 0x00;
byte mpu_address = 0x00;

/**********************************************
  Gyroscopes, accelerometers and magnetometers data variables
 **********************************************/
int16_t mx, my, mz, mx_out, my_out, mz_out, mx_prev, my_prev, mz_prev;
int16_t ax, ay, az, ax_out, ay_out, az_out, ax_prev, ay_prev, az_prev;
int16_t gx, gy, gz, gx_out, gy_out, gz_out, gx_prev, gy_prev, gz_prev;

/**********************************************
  BME280 data variables
 **********************************************/
int16_t temp;
uint32_t pressure;
uint32_t hum;

/**********************************************
  NTC Thermistor data variable
 **********************************************/
int16_t ntc_temp;

/**********************************************
  Battery voltage ADC variable
 **********************************************/
uint16_t batt_adc;

/**********************************************
  GNSS data variables and objecets
 **********************************************/
TinyGPSPlus gps;
float lon = 49.126;
float lat = 18.025;
uint16_t alt = 215;
uint8_t sats = 0;
uint8_t fix = 0;

/**********************************************
  SD Card
 **********************************************/
File dataFile;

/**********************************************
  Radio system variables and objects
 **********************************************/
String sCommand;
RFM95 radio = new Module(LORA_NSS, LORA_DIO0, LORA_RST, 40);
SimpleCLI radioCli;
SimpleCLI serialCli;

/**********************************************
  Data transmitted/written
 **********************************************/
typedef struct __attribute__((packed)) {
  int16_t gx;
  int16_t gy;
  int16_t gz;
  int16_t ax;
  int16_t ay;
  int16_t az;
  int16_t mx;
  int16_t my;
  int16_t mz;
  uint32_t pressure;
  int16_t temp;
  int16_t ntc_temp;
  uint16_t batt_adc;
  int16_t sol_mW_A;
  int16_t sol_mW_B;
  int16_t sol_mW_C;
  int16_t sol_mW_D;
  float lat;
  float lon;
  uint16_t alt;
  uint8_t sats;
  uint8_t fix;

  uint16_t crc;
} payload_t;

payload_t payload_struct = { 0 };

/**********************************************
  Support functions
 **********************************************/

void sensors_ids() {
  //availiable addresses
  byte error = 0;
  byte address = 0;
  byte id = 0;

  for (address = 1; address < 127; address++) {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0) {
      Serial.print("I2C sensor at 0x");
      if (address < 16) {
        Serial.print("0");
      }
      Serial.println(address, HEX);

      //BMP/BME identification
      if (address == 0x76 || address == 0x77) {
        Wire.beginTransmission(address);
        Wire.write(0xD0);
        Wire.endTransmission();
        Wire.requestFrom((uint8_t)address, (uint8_t)1);
        if (Wire.available() >= 1) {
          id = Wire.read();
        }

        if (id == 0x55) {
          bmp = true;
          bmp_address = address;
        } else if (id == 0x58) {
          bmp = true;
          bmp_address = address;
        } else if (id == 0x60) {
          bme = true;
          bme280_address = address;
        }
        Serial.print("Id of BMP/BME sensor ");
        Serial.println(id, HEX);
      }
      else if (address == 0x68 || address == 0x69) {  //MPU6050 identification
        Wire.beginTransmission(address);
        Wire.write(0x75);
        Wire.endTransmission();
        Wire.requestFrom((uint8_t)address, (uint8_t)1);
        if (Wire.available() >= 1) {
          id = Wire.read();
        }
        Serial.print("Id of MPU sensor ");
        if (id == 0x68) {
          mpu = true;
          mpu_address = address;
        } else {
          Serial.println("Unknown type of sensor");
        }
        Serial.println(id, HEX);
      }
      else if (address >= 0x40 && address <= 0x4F) { // INA219 solar input sensors
        Serial.print("INA219 probably at 0x");
        Serial.println(address, HEX);
      }
      else { //HMC/QMC identification
        byte read_error;
        Wire.beginTransmission(address);
        Wire.write(0x0D);
        read_error = Wire.endTransmission();
        if (read_error == 0) {
          Wire.requestFrom((uint8_t)address, (uint8_t)1);
          if (Wire.available() >= 1) {
            id = Wire.read();
          }
          if (id == 0xFF) {
            qmc = true;
            qmc_address = address;
          }
          Serial.print("Id of found sensor ");
          Serial.println(id, HEX);
        } else {
          Wire.beginTransmission(address);
          Wire.write(0x0A);
          read_error = Wire.endTransmission();
          if (read_error == 0) {
            if (Wire.available() >= 1) {
              id = Wire.read();
            }
            if (id == 0x48) {
              hmc = true;
              hmc_address = address;
              Serial.print("Id of HMC sensor ");
              Serial.println(id, HEX);
            }
            Serial.print("Id of HMC sensor ");
            Serial.println(id, HEX);
          } else {
            Serial.println("Unknown type of sensor");
          }
        }
      }
    }
  }
}

unsigned short crc16(char *data_p, unsigned short length) {
  unsigned char i;
  unsigned int data;
  unsigned int crc = 0xffff;

  if (length == 0) {
    return (~crc);
  }

  do {
    for (i = 0, data = (unsigned int)0xff & *data_p++; i < 8; i++, data >>= 1) {
      if ((crc & 0x0001) ^ (data & 0x0001)) {
        crc = (crc >> 1) ^ CRC_16_POLY;
      } else {
        crc >>= 1;
      }
    }
  } while (--length);

  crc = ~crc;
  data = crc;
  crc = (crc << 8) | (data >> 8 & 0xff);

  return (crc);
}


void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar != '\r') {
      sCommand += inChar;
    }
    if (inChar == '\n') {
      Serial.print("# ");
      Serial.println(sCommand);
      serialCli.parse(sCommand);
      sCommand = "";
    }
  }
}

void make_payload_struct() {
  payload_struct.gx = gx;
  payload_struct.gy = gy;
  payload_struct.gz = gz;
  payload_struct.ax = ax;
  payload_struct.ay = ay;
  payload_struct.az = az;
  payload_struct.mx = mx;
  payload_struct.my = my;
  payload_struct.mz = mz;
  payload_struct.pressure = pressure;
  payload_struct.temp = temp;
  payload_struct.ntc_temp = ntc_temp;
  payload_struct.batt_adc = batt_adc;
  if (solar) {
    payload_struct.sol_mW_A = (int16_t)solar_get_power_mW(1);
    payload_struct.sol_mW_B = (int16_t)solar_get_power_mW(2);
    payload_struct.sol_mW_C = (int16_t)solar_get_power_mW(3);
    payload_struct.sol_mW_D = (int16_t)solar_get_power_mW(4);
  }
  payload_struct.lat = lat;
  payload_struct.lon = lon;
  payload_struct.alt = alt;
  payload_struct.sats = sats;
  payload_struct.fix = fix;
}

/**********************************************
  Initial setup of satellite - runs once when powered on
 **********************************************/
void setup() {
  initCmds();
  Wire.begin();
  LoRa_previousMillis = 0;
  serial_previousMillis = 0;

  Serial.begin(115200);  //Serial COM port enable
  Serial3.begin(9600);   // GPS serial port init

  sensors_ids();  // determine sensors present and their addreses

  //init of all found sensors ant transmitter
  if (mpu) {
    init_MPU(mpu_address);
  }

  if (bme) {
    init_BME(bme280_address);
  }

  if (bmp) {
    init_BMP(bmp_address);
  }

  if (hmc) {
    init_hmc(hmc_address);
  }

  if (qmc) {
    init_qmc(qmc_address);
  }

  if (!SD.begin(SD_SS)) {
    Serial.println("SD initialization failed!");
  } else {
    Serial.println("SD initialization success!");
    sd_card = true;
  }

  init_LoRa();

  solar = solar_init();

  //initial csv

  if (sd_card && !SD.exists("data.csv")) {
    dataFile = SD.open("data.csv", FILE_WRITE);

    dataFile.print("uptime");
    dataFile.print(",");
    dataFile.print("gyro_x");
    dataFile.print(",");
    dataFile.print("gyro_y");
    dataFile.print(",");
    dataFile.print("gyro_z");
    dataFile.print(",");
    dataFile.print("acc_x");
    dataFile.print(",");
    dataFile.print("acc_y");
    dataFile.print(",");
    dataFile.print("acc_z");
    dataFile.print(",");
    dataFile.print("mag_x");
    dataFile.print(",");
    dataFile.print("mag_y");
    dataFile.print(",");
    dataFile.print("mag_z");
    dataFile.print(",");
    dataFile.print("bme280_press");
    dataFile.print(",");
    dataFile.print("bme280_temp");
    dataFile.print(",");
    dataFile.print("ntc_temp");
    dataFile.print(",");
    dataFile.print("batt_adc");
    dataFile.print(",");
    dataFile.print("SolXp");
    dataFile.print(",");
    dataFile.print("SolXn");
    dataFile.print(",");
    dataFile.print("SolYp");
    dataFile.print(",");
    dataFile.print("SolYn");
    dataFile.print(",");
    dataFile.print("lat");
    dataFile.print(",");
    dataFile.print("lon");
    dataFile.print(",");
    dataFile.print("alt");
    dataFile.print(",");
    dataFile.print("sats");
    dataFile.print(",");
    dataFile.println("fix");

    dataFile.close();
  }

  //set LEDs as output
  pinMode(RED_LED, OUTPUT);
  pinMode(ORANGE_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);

  //blink on startup
  for (int i = 0; i < 3; i++) {
    digitalWrite(RED_LED, HIGH);
    delay(100);
    digitalWrite(ORANGE_LED, HIGH);
    delay(100);
    digitalWrite(GREEN_LED, HIGH);
    delay(500);
    digitalWrite(RED_LED, LOW);
    delay(100);
    digitalWrite(ORANGE_LED, LOW);
    delay(100);
    digitalWrite(GREEN_LED, LOW);
    delay(500);
  }
  //disable LEDs on startup
  digitalWrite(RED_LED, LOW);
  digitalWrite(ORANGE_LED, LOW);
  digitalWrite(GREEN_LED, LOW);

  // Print CSV header

  Serial.print("uptime");
  Serial.print(",");
  Serial.print("gyro_x");
  Serial.print(",");
  Serial.print("gyro_y");
  Serial.print(",");
  Serial.print("gyro_z");
  Serial.print(",");
  Serial.print("acc_x");
  Serial.print(",");
  Serial.print("acc_y");
  Serial.print(",");
  Serial.print("acc_z");
  Serial.print(",");
  Serial.print("mag_x");
  Serial.print(",");
  Serial.print("mag_y");
  Serial.print(",");
  Serial.print("mag_z");
  Serial.print(",");
  Serial.print("bme280_press");
  Serial.print(",");
  Serial.print("bme280_temp");
  Serial.print(",");
  Serial.print("ntc_temp");
  Serial.print(",");
  Serial.print("batt_adc");
  Serial.print(",");
  if (solar) {
    Serial.print("sol_mW_A");
    Serial.print(",");
    Serial.print("sol_mW_B");
    Serial.print(",");
    Serial.print("sol_mW_C");
    Serial.print(",");
    Serial.print("sol_mW_D");
    Serial.print(",");
  }
  Serial.print("lat");
  Serial.print(",");
  Serial.print("lon");
  Serial.print(",");
  Serial.print("alt");
  Serial.print(",");
  Serial.print("sats");
  Serial.print(",");
  Serial.println("fix");
}

void loop() {
  receive_LoRa();

  while (Serial3.available()) {
    char c = Serial3.read();
    gps.encode(c);
  }

  if (bmp && !bme) {
    bmp_getPT(&pressure, &temp);
  } else if (bme) {
    bme_getPT(&pressure, &temp);
  }

  if (mpu) {
    getAccel(&ax, &ay, &az);
    getGyro(&gx, &gy, &gz);
  }

  if (qmc) {
    get_qmc(&mx, &my, &mz);
  }

  if (hmc) {
    get_hmc(&mx, &my, &mz);
  }

  getGPS(&lat, &lon, &alt, &sats, &fix);

  ntc_temp = ntc_readTemperature();

  batt_adc = analogRead(A0);

  make_payload_struct();

  unsigned long currentMillis = millis();
  if (currentMillis - serial_previousMillis >= serial_interval) {
    serial_previousMillis = currentMillis;

    Serial.print((long)(millis() / 1000));
    Serial.print(",");
    Serial.print(gx);
    Serial.print(",");
    Serial.print(gy);
    Serial.print(",");
    Serial.print(gz);
    Serial.print(",");
    Serial.print(ax);
    Serial.print(",");
    Serial.print(ay);
    Serial.print(",");
    Serial.print(az);
    Serial.print(",");
    Serial.print(mx);
    Serial.print(",");
    Serial.print(my);
    Serial.print(",");
    Serial.print(mz);
    Serial.print(",");
    Serial.print(pressure);
    Serial.print(",");
    Serial.print(temp);
    Serial.print(",");
    Serial.print(ntc_temp);
    Serial.print(",");
    Serial.print(batt_adc);
    Serial.print(",");
    if (solar) {
      Serial.print(solar_get_power_mW(1));
      Serial.print(",");
      Serial.print(solar_get_power_mW(2));
      Serial.print(",");
      Serial.print(solar_get_power_mW(3));
      Serial.print(",");
      Serial.print(solar_get_power_mW(4));
      Serial.print(",");
    }
    Serial.print(lat, 6);
    Serial.print(",");
    Serial.print(lon, 6);
    Serial.print(",");
    Serial.print(alt);
    Serial.print(",");
    Serial.print(sats);
    Serial.print(",");
    Serial.println(fix);

    if (sd_card) {
      dataFile = SD.open("data.csv", FILE_WRITE);

      dataFile.print((long)(millis() / 1000));
      dataFile.print(",");
      dataFile.print(gx);
      dataFile.print(",");
      dataFile.print(gy);
      dataFile.print(",");
      dataFile.print(gz);
      dataFile.print(",");
      dataFile.print(ax);
      dataFile.print(",");
      dataFile.print(ay);
      dataFile.print(",");
      dataFile.print(az);
      dataFile.print(",");
      dataFile.print(mx);
      dataFile.print(",");
      dataFile.print(my);
      dataFile.print(",");
      dataFile.print(mz);
      dataFile.print(",");
      dataFile.print(pressure);
      dataFile.print(",");
      dataFile.print(temp);
      dataFile.print(",");
      dataFile.print(ntc_temp);
      dataFile.print(",");
      dataFile.print(batt_adc);
      dataFile.print(",");
      if (solar) {
        dataFile.print(solar_get_power_mW(1), 2);
        dataFile.print(",");
        dataFile.print(solar_get_power_mW(2), 2);
        dataFile.print(",");
        dataFile.print(solar_get_power_mW(3), 2);
        dataFile.print(",");
        dataFile.print(solar_get_power_mW(4), 2);
        dataFile.print(",");
      }
      dataFile.print(lat, 6);
      dataFile.print(",");
      dataFile.print(lon, 6);
      dataFile.print(",");
      dataFile.print(alt);
      dataFile.print(",");
      dataFile.print(sats);
      dataFile.print(",");
      dataFile.println(fix);

      dataFile.close();
    }
  }

  currentMillis = millis();
  if (currentMillis - LoRa_previousMillis >= LoRa_interval && lora && LoRa_beacon_on) {
    LoRa_previousMillis = currentMillis;

    uint16_t i_crc = crc16((char *)&payload_struct, sizeof(payload_struct) - 2);
    payload_struct.crc = i_crc;
    send_LoRa((uint8_t *)&payload_struct, sizeof(payload_struct));
  }
}
