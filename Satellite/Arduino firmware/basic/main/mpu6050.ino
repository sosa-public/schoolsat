#include "config.h"

byte _mpu_address;

const int GYRO_X_OFFSET = -4;
const int GYRO_Y_OFFSET = 15;
const int GYRO_Z_OFFSET = -7;

const int ACC_X_OFFSET = 16;
const int ACC_Y_OFFSET = 48;
const int ACC_Z_OFFSET = 138;

void init_MPU(byte mpu_address)
{
  _mpu_address = mpu_address;
  Serial.print("MPU6050 init started...");
  
  write8(_mpu_address, 0x6B, 0x00);
  write8(_mpu_address, 0x1B, MPU_ACCEL_RESOLUTION);
  write8(_mpu_address, 0x1C, MPU_ACCEL_RESOLUTION);

  Serial.println("DONE");
}

/**********************************************
 Accelerometer data reading
 **********************************************/

void getAccel(int16_t* Ax, int16_t* Ay, int16_t* Az)
{
  *Ax = readS16(_mpu_address, 0x3B) - ACC_X_OFFSET;
  *Ay = readS16(_mpu_address, 0x3D) - ACC_Y_OFFSET;
  *Az = readS16(_mpu_address, 0x3F) - ACC_Z_OFFSET;
}

void getGyro(int16_t* Gx, int16_t* Gy, int16_t* Gz)
{
  *Gx = readS16(_mpu_address, 0x43) - GYRO_X_OFFSET;
  *Gy = readS16(_mpu_address, 0x45) - GYRO_Y_OFFSET;
  *Gz = readS16(_mpu_address, 0x47) - GYRO_Z_OFFSET;
}
